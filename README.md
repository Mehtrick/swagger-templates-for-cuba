# Swagger-Templates for cuba


[![forthebadge](http://forthebadge.com/images/badges/60-percent-of-the-time-works-every-time.svg)](http://forthebadge.com)[![forthebadge](http://forthebadge.com/images/badges/makes-people-smile.svg)](http://forthebadge.com)[![forthebadge](http://forthebadge.com/images/badges/contains-technical-debt.svg)](http://forthebadge.com)[![forthebadge](http://forthebadge.com/images/badges/built-with-love.svg)](http://forthebadge.com)

## Ziel
Schnittstellen Generierung aus Swagger in JAX-RS inklusive rudimentäre Anbindung an das Framework cuba

## Aufbau 

### Api.java 
- RestEndpoint der Schnittstelle
- beiinhaltet Basisprüfungen von Eingabeparametern (Pflichtfeld oder nicht)
- sucht über die entsprechende ServiceFactory die Cuba-Komponente
- führt die Methode der cuba Komponente in dem jeweiligen Endpoint aus

### ServiceFactory.java
- Stellt die Verbindung mit Cuba her indem ein neuer CubaClient initialisiert wird
- Sucht anhand des CubaClient die Komponente zur ServiceFactory heraus

### ApiSerice.java
- wird über die ServiceFactory an die Api weitergeleitet
- Interface der zu implementierenden Komponente
- wird mit '@External' annotiert
- Diese Komponente muss vom Entwickler implementiert werden. Der Name der Implementierenden-Komponente **muss** so lauten wie der Klassenname des Interfaces

### ModelKlassen
- liegen unter dem Ordner Model
- Werden über die Schnittstelle rein und raus gegeben
- beinhalten Validierungen 
- Jede Model Klasse kann über ein Builder-Pattern gebaut werden

![Modell](https://www.websequencediagrams.com/cgi-bin/cdraw?lz=dGl0bGUgU3dhZ2dlci1UZW1wbGF0ZXMgZm9yIEN1YmEKCkFwaS0-U2VydmljZUZhY3Rvcnk6IGluaXQoKQoACQ4AERZpYWxpc2llcmVDdWJhQ2xpZW4AJhRBcGk6IDoAWQ4AZRZzdWNoZUtvbXBvbmVudGUAYyMAIhBBcGkAgUkHX05hbWUAgTcQIC0-IACBCQZDdWJhLQBhCgCCBQYABg86IGRvKE1vZGVsS2xhc3NlKQoAJw8ARQpSZXNwb25zZTwAJAs-CgoK&s=earth)

## Was muss der Entwickler tun um die Schnittstelle zu implementieren
- ApiService Interface implementieren
- Der Komponenten Name **muss** dem Namen der Interface Klasse entsprechen
- Komponente in die Wired Jar aufnehmen
- Tests nicht Vergessen!

## Wo liegt die swagger.yaml?

Im Root Ordner :) 

## Wie generiere ich die Klassen aus der Schnittstelle?
Über die Konsole via
```
gradlew gen 
```

Die generierten Klassen liegen dann unter src/main/java

## Todos und offene Fragen
Finden sich [hier](https://bitbucket.org/Mehtrick/swagger-templates-for-cuba/issues?status=new&status=open)